#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow *w = nullptr;

void selectionSort(Array &toSort) {
    for (size_t i = 0; i < toSort.size(); i++) {
        // Recherche du minimum
        int minimum = i;

        for (size_t j = i + 1; j < toSort.size(); j++) {
            if (toSort[minimum] > toSort[j]) {
                minimum = j;
            }
        }

        // On inverse le minimum avec l'index actuel
        toSort.swap(minimum, i);
    }
}

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    uint elementCount = 15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

    return a.exec();
}
