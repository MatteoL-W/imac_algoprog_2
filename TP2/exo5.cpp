#include <QApplication>
#include <time.h>
#include <iostream>
#include "tp2.h"

MainWindow *w = nullptr;

void merge(Array &first, Array &second, Array &result);

void splitAndMerge(Array &origin) {
    // stop statement = condition + return (return stop the function even if it does not return anything)
    if (origin.size() <= 1) {
        return;
    }

    // initialisation
    Array &first = w->newArray(origin.size() / 2);
    Array &second = w->newArray(origin.size() - first.size());

    // split
    for (size_t i = 0; i < first.size(); i++) {
        first[i] = origin[i];
    }

    for (size_t i = 0; i < second.size(); i++) {
        second[i] = origin[i + first.size()];
    }

    // recursiv splitAndMerge of lowerArray and greaterArray
    splitAndMerge(first);
    splitAndMerge(second);

    // merge
    merge(first, second, origin);
}

void merge(Array &first, Array &second, Array &result) {
    size_t cursorFirst = 0, cursorSecond = 0, index = 0;

    for (size_t i = 0; i < result.size(); i++) {
        if (cursorFirst >= first.size() || cursorSecond >= second.size()) {
            break;
        }

        if (first[cursorFirst] < second[cursorSecond]) {
            result[i] = first[cursorFirst];
            cursorFirst++;
        } else {
            result[i] = second[cursorSecond];
            cursorSecond++;
        }

        index++;
    }

    for (size_t i = cursorFirst; i < first.size(); i++) {
        result[index++] = first[i];
    }
    for (size_t i = cursorSecond; i < second.size(); i++) {
        result[index++] = second[i];
    }

}

void mergeSort(Array &toSort) {
    splitAndMerge(toSort);
}

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
    w->show();

    return a.exec();
}
