#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return (nodeIndex * 2 + 1);
}

int Heap::rightChild(int nodeIndex)
{
    return (nodeIndex * 2 + 2);
}

//insère un nouveau noeud dans le tas heap tout en gardant la propriété de tas.
void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
    (*this)[i] = value;

    while (i > 0 && this->get(i) > this->get((i - 1) / 2)) {
        swap(i, (i - 1) / 2);
        i = (i - 1) / 2;
    }
}

// Si le noeud à l’indice nodeIndex n’est pas supérieur à ses enfants, reconstruit le tas à partir de cette index
void Heap::heapify(int heapSize, int nodeIndex)
{
    int largest = nodeIndex;

    if (leftChild(nodeIndex) < heapSize && get(largest) < get(leftChild(nodeIndex))) {
        largest = leftChild(nodeIndex);
    }

    if (rightChild(nodeIndex) < heapSize && get(largest) < get(rightChild(nodeIndex))) {
        largest = rightChild(nodeIndex);
    }

    if (largest != nodeIndex) {
        swap(nodeIndex, largest);
        heapify(heapSize, largest);
    }

}

// Construit un tas à partir des valeurs de numbers (vous pouvez utiliser soit insertHeapNode soit heapify)
void Heap::buildHeap(Array& numbers)
{
    for (size_t i = 0; i < numbers.size(); i++) {
        insertHeapNode(i, numbers[i]);
    }
}

// Construit un tableau trié à partir d’un tas heap
void Heap::heapSort()
{
    for (size_t i = this->size() - 1; i > 0; i--) {
        swap(0, i);
        heapify(i, 0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
