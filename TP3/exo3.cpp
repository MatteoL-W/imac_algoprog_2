#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow *w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node {
    SearchTreeNode *left;
    SearchTreeNode *right;
    int value;

    void initNode(int value) {
        // init initial node without children
        this->value = value;
        this->right = NULL;
        this->left = NULL;
    }

    void insertNumber(int value) {
        // create a new node and insert it in right or left child

        if (value < this->value) {
            if (this->left) {
                this->left->insertNumber(value);
            } else {
                SearchTreeNode *newNode = new SearchTreeNode(value);
                newNode->initNode(value);
                this->left = newNode;
            }
        } else {
            if (this->right) {
                this->right->insertNumber(value);
            } else {
                SearchTreeNode *newNode = new SearchTreeNode(value);
                newNode->initNode(value);
                this->right = newNode;
            }
        }
    }

    uint height() const {
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        uint leftHeight = 1, rightHeight = 1, height = 1;

        if (this->left) {
            leftHeight += this->left->height();
        }

        if (this->right) {
            rightHeight += this->right->height();
        }

        height = (rightHeight > leftHeight) ? rightHeight : leftHeight;

        return height;
    }

    uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        uint nodesCounter = 1;

        if (this->right) {
            nodesCounter += this->right->nodesCount();
        }

        if (this->left) {
            nodesCounter += this->left->nodesCount();
        }

        return nodesCounter;

    }

    bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        return (!this->left && !this->right);
    }

    void allLeaves(Node *leaves[], uint &leavesCount) {
        // fill leaves array with all leaves of this tree
        // 1, 6, 10, 20

        if (this->isLeaf()) {
            leaves[leavesCount] = this;
            leavesCount++;
        } else {
            if (this->right) {
                this->right->allLeaves(leaves, leavesCount);
            }

            if (this->left) {
                this->left->allLeaves(leaves, leavesCount);
            }
        }
    }

    void inorderTravel(Node *nodes[], uint &nodesCount) {
        // fill nodes array with all nodes with inorder travel

        if (this->left) {
            this->left->inorderTravel(nodes, nodesCount);
        }

        nodes[nodesCount] = this;
        nodesCount++;

        if (this->right) {
            this->right->inorderTravel(nodes, nodesCount);
        }
    }

    void preorderTravel(Node *nodes[], uint &nodesCount) {
        nodes[nodesCount] = this;
        nodesCount++;

        if (this->left) {
            this->left->preorderTravel(nodes, nodesCount);
        }

        if (this->right) {
            this->right->preorderTravel(nodes, nodesCount);
        }

    }

    void postorderTravel(Node *nodes[], uint &nodesCount) {
        // fill nodes array with all nodes with postorder travel

        if (this->left) {
            this->left->postorderTravel(nodes, nodesCount);
        }

        if (this->right) {
            this->right->postorderTravel(nodes, nodesCount);
        }

        nodes[nodesCount] = this;
        nodesCount++;
    }

    // insertNumber c'est el roatet

    Node *find(int value) {
        Node *node = nullptr;

        if (value == this->value) {
            node = this;
        } else if (value < this->value) {
            node = this->left->find(value);
        } else if (value > this->value) {
            node = this->right->find(value);
        }

        return node;
    }

    void reset() {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) { initNode(value); }

    ~SearchTreeNode() {}

    int get_value() const { return value; }

    Node *get_left_child() const { return left; }

    Node *get_right_child() const { return right; }
};

Node *createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
    w->show();

    return a.exec();
}
