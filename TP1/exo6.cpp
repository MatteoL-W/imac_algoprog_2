#include <iostream>

using namespace std;

struct Noeud {
    int donnee;
    Noeud *suivant;
};

struct Liste {
    Noeud *premier;
};

struct DynaTableau {
    int *donnees;
    int capacite;
    int size;
};


void initialise(Liste *liste) {
    liste->premier = nullptr;
}

bool est_vide(const Liste *liste) {
    if (liste->premier == nullptr) {
        return true;
    }
    return false;
}

void ajoute(Liste *liste, int valeur) {
    Noeud *currentNode = liste->premier;
    Noeud *newNode = new Noeud();

    if (!newNode) {
        std::cout << "Allocation failed" << std::endl;
    }

    newNode->donnee = valeur;

    if (currentNode == nullptr) {
        liste->premier = newNode;
    } else {
        while (currentNode->suivant != nullptr) {
            currentNode = currentNode->suivant;
        }
        currentNode->suivant = newNode;
    }
}

void affiche(const Liste *liste) {
    int iterator = 0;
    Noeud *currentNode = liste->premier;
    while (currentNode->suivant != nullptr) {
        std::cout << "Valeur du noeud n°" << iterator << " = " << currentNode->donnee << std::endl;
        iterator++;
        currentNode = currentNode->suivant;
    }
}

int recupere(const Liste *liste, int n) {
    int iterator = 0;
    Noeud *currentNode = liste->premier;
    while (currentNode->suivant != nullptr && iterator < n) {
        iterator++;
        currentNode = currentNode->suivant;
    }
    return currentNode->donnee;
}

int cherche(const Liste *liste, int valeur) {
    int iterator = 0;
    Noeud *currentNode = liste->premier;

    while (currentNode->suivant != nullptr) {
        if (currentNode->donnee == valeur) {
            return iterator;
            break;
        }
        currentNode = currentNode->suivant;
        iterator++;
    }

    return -1;
}

void stocke(Liste *liste, int n, int valeur) {
    Noeud *aimNode = new Noeud();
    if (!aimNode) {
        std::cout << "Allocation failed";
    }
    aimNode->donnee = valeur;

    int iterator = 0;
    Noeud *currentNode = liste->premier;

    while (currentNode != nullptr) {
        if (iterator == n - 1) {
            aimNode->suivant = currentNode->suivant;
            currentNode->suivant = aimNode;
        }
        iterator++;
        currentNode = currentNode->suivant;
    }
}

void ajoute(DynaTableau *tableau, int valeur) {
    if (tableau->capacite < tableau->size + 1) {
        tableau->capacite += 3;
        // pas fan...
        int *newTab = new int(tableau->capacite);
        if (!newTab) {
            std::cout << "Allocation failed";
        }
        for (int i = 0; i < tableau->size; i++) {
            newTab[i] = tableau->donnees[i];
        }
        tableau->donnees = newTab;
    }

    tableau->donnees[tableau->size] = valeur;
    tableau->size += 1;
}


void initialise(DynaTableau *tableau, int capacite) {
    tableau->capacite = capacite;
    tableau->size = 0;
    tableau->donnees = new int[capacite];
}

bool est_vide(const DynaTableau *liste) {
    if (liste->size == 0) {
        return true;
    }
    return false;
}

void affiche(const DynaTableau *tableau) {
    for (int i = 0; i < tableau->size; i++) {
        std::cout << "Valeur n°" << i << " du tableau = " << tableau->donnees[i] << std::endl;
    }
}

int recupere(const DynaTableau *tableau, int n) {
    if (tableau->size > n) {
        return tableau->donnees[n];
    }
    return -1;
}

int cherche(const DynaTableau *tableau, int valeur) {
    for (int i = 0; i < tableau->size; i++) {
        if (tableau->donnees[i] == valeur) {
            return i;
        }
    }
    return -1;
}

void stocke(DynaTableau *tableau, int n, int valeur) {
    if (tableau->size > n) {
        tableau->donnees[n] = valeur;
    }
}

//void pousse_file(DynaTableau* liste, int valeur)
// file = on stocke à la fin
void pousse_file(Liste *liste, int valeur) {
    ajoute(liste, valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste *liste) {
    Noeud *currentNode = liste->premier;
    if (!currentNode) {
        std::cout << "Allocation failed";
    }

    if (liste->premier->suivant == NULL) {
        int valeur = liste->premier->donnee;
        liste->premier = NULL;
        return valeur;
    }

    while (currentNode->suivant != nullptr && currentNode->suivant->suivant != nullptr) {
        currentNode = currentNode->suivant;
    }

    int valeur = currentNode->suivant->donnee;
    delete currentNode->suivant;
    currentNode->suivant = nullptr;
    return valeur;
}

//void pousse_pile(DynaTableau* liste, int valeur)
// pile = on stocke avant
void pousse_pile(Liste *liste, int valeur) {
    Noeud *newNode = new Noeud;
    if (!newNode) {
        std::cout << "Allocation failed";
    }

    newNode->donnee = valeur;
    newNode->suivant = liste->premier;
    liste->premier = newNode;
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste *liste) {
    if (liste->premier != nullptr) {
        int valeur = liste->premier->donnee;
        liste->premier = liste->premier->suivant;
        return valeur;
    }
    return 0;
}


int main() {
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste)) {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau)) {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i = 1; i <= 7; i++) {
        ajoute(&liste, i * 7);
        ajoute(&tableau, i * 5);
    }

    if (est_vide(&liste)) {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau)) {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i = 1; i <= 7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while (!est_vide(&file) && compteur > 0) {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0) {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while (!est_vide(&pile) && compteur > 0) {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0) {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
