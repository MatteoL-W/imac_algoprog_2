#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point) {
    float tempX = z.x;
    float tempY = z.y;

    // z au carré
    z.x = (tempX * tempX - tempY * tempY);
    z.y = 2 * tempX * tempY;

    // z² + point
    z.x += point.x;
    z.y += point.y;

    float module = sqrt(z.x * z.x + z.y * z.y);

    if (module > 2) {
        return 1;
    } else if (n == 0) {
        return 0;
    }

    return isMandelbrot(z, n - 1, point);
}

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    MainWindow *w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



